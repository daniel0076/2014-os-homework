#include <iostream>
#include <cstdlib>
#include <fstream>
#include <iomanip>
#include <cstring>
using namespace std;
class process  //class for process
{
    public:
        process(string name="P0",int arrive=0,int burst=0):arrive_t(arrive),burst_t(burst),burst_r(burst),name(name){};
        const int arrive_time(){ return arrive_t;};
        const int burst_time() { return burst_t;};
        int& burst_remain() { return burst_r;};
        const string proc_name() {return name;};
    private:
        string name;
        int arrive_t;
        int burst_t;
        int burst_r;
};
struct node   //the node of the process
{
    process* proc;
    node* next;
};
class process_queue  //use linked-list to make a queue of process
{
    public:
        process_queue():first(NULL),current(first){};
        void push(string,int,int);
        void push(const process&);
        void pop();
        bool empty();
        void print();
        process get_proc();  //get the process from the front of the queue
        int  next_preempt(int,int); //find the next process which will preempt the current process
    private:
        node* first;
        node* current;
};
class scheduler
{
    public:
        scheduler():run_t(0),waiting_t(0),turnaround_t(0),process_count(0){};  //the cpu_schduler
        void load(string);
        void fcfs(); //first come first serve
        void srtf(); //shotedt remaing time first
        void rr(int); //round robin with time quantum
        void reset(); //reset everything
        void avg_print(double,double,int);   //the printer haha ^^
    private:
        process_queue ready;  //ready queuq
        process_queue proc_list;  //process list
        int run_t,waiting_t,turnaround_t;
        int process_count;
        fstream fp; //output file
};
int main()
{
    scheduler cpu_schduler;
    string input;
    cout<<"Please input file name or \"quit\" to quit"<<endl;
    cout<<"File name: ";
    while(cin>>input)  //a toy user interface
    {
        if(!strcmp(input.c_str(),"quit")){
            cout<<"Bye!"<<endl;
            break;
        }
        else {
            cpu_schduler.load(input);
        }
        cout<<endl<<"Please input file name or \"quit\" to quit"<<endl;
        cout<<"File name: ";
    }
    return 0;
}

//process_queue class method implementation
void process_queue::push(string name,int arrive,int burst)
{
    if(first==NULL){
        first=new node;
        first->proc=new process(name,arrive,burst);
        current=first;
        first->next=NULL;
    }else
    {
        current->next=new node;
        current->next->proc=new process(name,arrive,burst);
        current=current->next;
        current->next=NULL;
    }
}
void process_queue::push(const process& proc)
{
    if(first==NULL){
        first=new node;
        first->proc=new process(proc);
        current=first;
        first->next=NULL;
    }else
    {
        current->next=new node;
        current->next->proc=new process(proc);
        current=current->next;
        current->next=NULL;
    }
}
void process_queue:: pop()
{
    if(!empty())
    {
        node* tmp=first;
        first=first->next;
        delete tmp->proc;
        delete tmp;
    }
}
process process_queue:: get_proc()
{
    return *first->proc;
}
int process_queue::next_preempt(int run_t,int burst_t)
{
    node* it=first;
    while(it!=NULL)
    {
            //if next process has short burst time and is will arrive before the current process finished
        if(it->proc->arrive_time()<=run_t+burst_t && it->proc->burst_time()<burst_t){
            //cout<<"preempted "<<it->proc->proc_name()<<" "<<it->proc->arrive_time()<<" "<<run_t+burst_t; //debug
            //cout<<" &&  "<<it->proc->burst_time()<<" "<<burst_t<<endl; //debug
            return it->proc->arrive_time(); //return the next preempted process's arrive time,stupid bug
        }
        it=it->next;
    }
    return -1;
}
bool process_queue::empty()
{
    return first==NULL;
}
void process_queue::print()
{
    if(!empty())
    {
        cout<<get_proc().proc_name()<<" ";
        cout<<get_proc().arrive_time()<<" ";
        cout<<get_proc().burst_time()<<endl;
    }else cout<<"empty"<<endl;
}
//scheduler class method
void scheduler::reset()
{
    turnaround_t=0;
    run_t=0;
    process_count=0;
    waiting_t=0;
    while(!proc_list.empty()) proc_list.pop();
    while(!ready.empty()) ready.pop();
}
void scheduler::avg_print(double waiting_t,double turnaround_t,int count)
{
    fp<<setprecision(3)<<"Average Waiting Time: "<<waiting_t/count<<endl;
    fp<<"Average Turnaround Time: "<<turnaround_t/count<<endl;
    cout<<endl<<"The result has been save to 0216007.txt"<<endl;
    fp.close();
}
void scheduler::load(string fname)
{
    int quantum=0,arrive_t=0,burst_t=0;
    char tmp[100],method[10],name[10];
    fstream file;
    file.open(fname.c_str(),ios::in);
    fp.open("0216007.txt",ios::out);
    if(file && fp)
    {
        file.getline(tmp,100);
        sscanf(tmp,"Scheduling Algorithm: %s %d",method,&quantum);
        //cout<<method<<" "<<quantum<<endl; //debug
        file.getline(tmp,100);
        while(!file.eof())  //load the process in the file
        {
            file.getline(tmp,100);
            sscanf(tmp,"%s",name);
            file.getline(tmp,100);
            sscanf(tmp,"Arrival Time: %d",&arrive_t);
            file.getline(tmp,100);
            sscanf(tmp,"Burst Time: %d",&burst_t);
            file.getline(tmp,100);
            //cout<<name<<" "<<arrive_t<<" "<<burst_t<<endl; //debug
            proc_list.push(name,arrive_t,burst_t); //push to process list
        }
        file.close();
        if(!strcmp(method,"RR")) rr(quantum);
        else if(!strcmp(method,"SRTF")) srtf();
        else if(!strcmp(method,"FCFS")) fcfs();
        else {
            fp.close();
            file.close();
            reset();
            cout<<endl<<"Input data error,can't find schedule method"<<endl;
        }
    }else
    {
        fp.close();
        file.close();
        cout<<endl<<"File open error"<<endl<<"please try again or restart the program"<<endl;
    }
}
void scheduler::fcfs()
{
    process exec;
    while(!proc_list.empty())
    {
        exec=proc_list.get_proc();
        if(exec.arrive_time()>run_t){ run_t=exec.arrive_time();}
        fp<<exec.proc_name()<<"\t"<<run_t<<"-"<<run_t+exec.burst_time()<<endl;
        waiting_t+=run_t-exec.arrive_time();
        run_t+=exec.burst_time();
        turnaround_t+=run_t-exec.arrive_time();
        process_count+=1;
        proc_list.pop();
    }
    avg_print(waiting_t,turnaround_t,process_count);
    reset();
}
void scheduler::rr(int q)
{
    process exec;
    while(!proc_list.empty())
    {
        ready.push(proc_list.get_proc());
        proc_list.pop();
        while(!ready.empty())
        {
            exec=ready.get_proc();
            ready.pop();
            if(exec.arrive_time()>run_t){ run_t=exec.arrive_time();}
            if(exec.burst_remain()>q)  //burst time is longer than quantum
            {
                fp<<exec.proc_name()<<"\t"<<run_t<<"-"<<run_t+q<<endl;
                exec.burst_remain()-=q;
                run_t+=q;
                while(!proc_list.empty()&&proc_list.get_proc().arrive_time()<=run_t){
                    ready.push(proc_list.get_proc());
                    proc_list.pop();
                }
                ready.push(exec);
            }else{ //burst time is lesser than quantum
                fp<<exec.proc_name()<<"\t"<<run_t<<"-"<<run_t+exec.burst_remain()<<endl;
                run_t+=exec.burst_remain();
                waiting_t+=run_t-exec.burst_time()-exec.arrive_time();
                turnaround_t+=run_t-exec.arrive_time();
                process_count+=1;
            }
        }
    }
    avg_print(waiting_t,turnaround_t,process_count);
    reset();
}
void scheduler::srtf()
{
    process exec;
    int next_arr_t=-1;
    while(!proc_list.empty())
    {
        ready.push(proc_list.get_proc());
        proc_list.pop();
        while(!ready.empty())
        {
            exec=ready.get_proc();
            ready.pop();
            while(ready.next_preempt(run_t,exec.burst_remain())>=0) //find the shortest remain time in the ready queue
            {
                ready.push(exec);
                exec=ready.get_proc();
                ready.pop();
            }
            next_arr_t=proc_list.next_preempt(run_t,exec.burst_remain());
            if(next_arr_t>=0)  //will be preempted
            {
                if(run_t!=next_arr_t){
                    fp<<exec.proc_name()<<"\t"<<run_t<<"-"<<next_arr_t<<endl;
                }
                exec.burst_remain()-=next_arr_t-run_t;
                run_t+=next_arr_t-run_t;
                while(!proc_list.empty()&&proc_list.get_proc().arrive_time()<=run_t){
                    ready.push(proc_list.get_proc());
                    proc_list.pop();
                }
                ready.push(exec);
            }else //will not be preempted
            {
                fp<<exec.proc_name()<<"\t"<<run_t<<"-"<<run_t+exec.burst_remain()<<endl;
                run_t+=exec.burst_remain();
                waiting_t+=run_t-exec.burst_time()-exec.arrive_time();
                turnaround_t+=run_t-exec.arrive_time();
                process_count+=1;
            }
        }
    }
    avg_print(waiting_t,turnaround_t,process_count);
    reset();
}
