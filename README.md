Intro to Operation System - NCTU
===
#### 2014-Fall NCTU Department of Computer Science
---

### Projects

+ CPU scheduler
    + inplement several kinds of CPU scheduling method
    + methods: FSFC,SRTF,RR
    + C++

+ Threading
    + create several threads to do large addings
    + using pthread and semaphore
    + avoid deadlock
    + C++
